<?php
session_start();

//constantes region
require_once("partials/constants.php");
//constantes endregion

//get asset region
function get_asset($tipo, $nombre, $bool_print = true) {
	if (isset($tipo) && isset($nombre)) {
		if ($bool_print) {
			echo get_stylesheet_directory_uri() . "/assets/" . $tipo . "/" . $nombre;
		} else {
			return get_stylesheet_directory_uri() . "/assets/" . $tipo . "/" . $nombre;
		}
	}
}
//get asset endregion

//setup region
if ( ! function_exists( 'playground_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function playground_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on playground, use a find and replace
	 * to change 'playground' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'playground', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'playground' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'playground_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	
 
   register_post_type( 'proyecto',
		array(
			'labels' => array(
				'name' => __( 'Proyectos' ),
				'singular_name' => __( 'proyecto' )
			),
			'hierarchical' => true,
			'public' => true,
			'has_archive' => true,
			'taxonomies' => array('category', 'post_tag'),
			'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'categories', 'taxonomies')
		)
	);
	
	
}
endif;
add_action( 'init', 'playground_setup' , 0);

function playground_post_types() {
	register_post_type( 'testimonio',
		array(
			'labels' => array(
			'name' => __( 'Testimonios' ),
			'singular_name' => __( 'testimonio' )
			),
		'hierarchical' => true,
		'taxonomies' => array('category', 'post_tag'),
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'categories'),
		)
	);
}
add_action( 'init', 'playground_post_types' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function playground_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'playground_content_width', 640 );
}
add_action( 'after_setup_theme', 'playground_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function playground_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'playground' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'playground' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'playground_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function playground_scripts() {
	wp_enqueue_style( 'playground-style', get_stylesheet_uri(), array(), assets_version );
	wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:400,700', array() );
	
	if (is_page_template("page-landing.php")) {
		wp_enqueue_style( 'fonts-landing', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700', array() );
	}

	//wp_enqueue_script( 'axios', 'https://unpkg.com/axios/dist/axios.min.js', array(), assets_version, true );
	if (vue_development) {
		wp_enqueue_script( 'vue-js-development', get_template_directory_uri() . '/js/vue.js', array(), assets_version, true );
	} else {
		wp_enqueue_script( 'vue-js-production', get_template_directory_uri() . '/js/vue.min.js', array(), assets_version, true );
	}
	
	wp_enqueue_script( 'axios', 'https://unpkg.com/axios/dist/axios.min.js', array(), false, true );
	wp_enqueue_script( 'base', get_template_directory_uri() . '/js/base.js', array(), assets_version, true );
	wp_enqueue_script( 'componentes', get_template_directory_uri() . '/js/componentes.js', array(vue_development ? "vue-js-development" : "vue-js-production", "base"), assets_version, true);
	wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.js', array(vue_development ? "vue-js-development" : "vue-js-production", "axios", "base", "componentes"), assets_version, true );
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array(), assets_version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'playground_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function filter_ptags_on_images($content) {
    // do a regular expression replace...
    // find all p tags that have just
    // <p>maybe some white space<img all stuff up to /> then maybe whitespace </p>
    // replace it with just the image tag...
    return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}

// we want it to be run after the autop stuff... 10 is default.
add_filter('the_content', 'filter_ptags_on_images');
//setup endregion

//ajustes del tema region
function anadir_item_menu() {
	add_menu_page("Ajustes del tema", "Ajustes del tema", "manage_options", "theme-panel", function () {
		echo '<div class="wrap">
			    <h1>Ajustes del tema</h1>
			    <form method="post" action="options.php">';

        settings_fields("common_section");
        do_settings_sections("theme-panel");
        submit_button();

		echo 	'</form>
				</div>';
	}, null, 99);
}

function agregar_campos_y_panel($campos) {
	add_settings_section("common_section", "Ajustes generales para el tema y sitio", null, "theme-panel");
	
	foreach ($campos as $id => $args) {
		add_settings_field($id, $args["title"], $args["callback"], $args["page"], $args["section"]);
		register_setting($args["section"], $id);
	}
}

function common_input($id, $type = "text") {
	echo "<input type='$type' name='$id' id='$id' value='" . get_option($id) . "'>";
}

add_action("admin_menu", "anadir_item_menu");

add_action("admin_init", function () {
	agregar_campos_y_panel(array(
		"ga_code" => array(
			"title" => "Código Seguimiento de Analytics",
			"callback" => function () { common_input("ga_code"); },
			"page" => "theme-panel",
			"section" => "common_section"
		),
		"custom_option_telefono" => array(
			"title" => "Telefono",
			"callback" => function () { common_input("custom_option_telefono"); },
			"page" => "theme-panel",
			"section" => "common_section"
		),
		"custom_option_whatsapp" => array(
			"title" => "Número de Whatsapp",
			"callback" => function () { common_input("custom_option_whatsapp"); },
			"page" => "theme-panel",
			"section" => "common_section"
		),
		"custom_option_instagram" => array(
			"title" => "Perfil de Instagram",
			"callback" => function () { common_input("custom_option_instagram"); },
			"page" => "theme-panel",
			"section" => "common_section"
		)
	));
});

function get_custom_options() {
	$prefix = "custom_option_";
	$conexion = conectar();
 
	$query = mysqli_query($conexion, 'SELECT option_name, option_value FROM `wp_options` WHERE option_name LIKE "' . $prefix . '%"');
	$output = array();

	if ($query) {
		while ($row = mysqli_fetch_assoc($query)) {
			$output[str_replace($prefix, "", $row["option_name"])] = $row["option_value"];
		}
		
		//try to parse js json objects if found
		foreach ($output as $key => $meta) {
			if (
				(substr($meta, 0, 1) === "[" && substr($meta, strlen($meta) - 1, 1) === "]") ||
				(substr($meta, 0, 1) === "{" && substr($meta, strlen($meta) - 1, 1) === "}")
			) {
			   try {
					$meta_js = json_decode($meta, true);
					$output[$key] = $meta_js;
				} catch(Exception $e) {
					//echo 'Message: ' .$e->getMessage();
				}
			}
		}
		
		return $output;
	}
	
	return false;
}

function mostrar_ga() {
	$codigo_ga = get_option("ga_code");
	if (isset($codigo_ga) && !empty($codigo_ga)) {
		if (!is_user_logged_in()) {
	?>
		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $codigo_ga; ?>', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->
	<?php
		}
	}
}

add_action("wp_head", "mostrar_ga", 1);
//endregion

//funciones region
function conectar($set_names_utf8 = true, $set_charset_utf8 = true) {
	$conexion = mysqli_connect("localhost", DB_USER, DB_PASSWORD, DB_NAME);
	
	if ($set_names_utf8) {
 		mysqli_query($conexion, "set names 'utf8'");
	}
	
	if ($set_charset_utf8) {
 		mysqli_set_charset($conexion, 'utf-8');
 		mysqli_query($conexion, 'set character set utf8');
	}
	
	return $conexion;
}
//funciones endregion

//microservicios region
function microServicio($query_string, $function_callback = false) {
	$conexion = conectar();
	$query = mysqli_query($conexion, $query_string);
	$salida = array();
	
	if ($query) {
		while ($row = mysqli_fetch_assoc($query)) {
			if ($function_callback) {
				$row = call_user_func($function_callback, $row);
				if ($row !== null) {
					$salida[] = $row;
				}
			} else {
				$salida[] = $row;
			}
		}
	}
	
	return $salida;
}

/*
function getServicios() {
	return microServicio("SELECT * FROM servicios");
}


function solicitarMicroServicios() {
	$sol = array();
	if (is_page_template("page-home.php")) {
		$sol[] = "servicios";
	}
	
	echo "\n<script>var solicitar_servicios = " . json_encode($sol) . ";</script>\n";
}

add_action("wp_footer", "solicitarMicroServicios");
*/
//microservicios endregion






















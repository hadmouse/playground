<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package playground
 */

get_header(); ?>

<div class="max-content text-center wp-404">
	<div class="wp-404_inner">
		<h2>No se ha encontrado lo que buscabas</h2>
		<p>No existe la página que buscabas. Si llegaste aquí por error, puedes volver al home haciendo click más abajo</p>
		<p>
			<a href="<?php echo get_home_url(); ?>" class="btn btn-color-1 btn-inline-block">
				Volver al Inicio
				<span class="tei tei-chevron-right"></span>
			</a>
		</p>
	</div>
</div>

<?php
get_footer();

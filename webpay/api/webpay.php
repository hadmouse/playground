<?php
//require_once('phpmailer/PHPMailerAutoload.php');

if (!isset($GLOBALS["conn"])) {
	$GLOBALS["conn"] = mysqli_connect(server, user, pass, db);
}

class WebPay
{
	var $id;
	var $OC;
	var $estado;
	var $monto;
	var $TBK_RESPUESTA;
	var $authCode;
	var $paymentTypeCode;
	var $cuotas;
	var $token;
	var $TBK_ORDEN_COMPRA;
	var $url;
	var $finalTarjeta;
	var $fecha;

	function __construct()
	{
		$this->id = -1;
		$this->OC = "";
		$this->estado = -1;
		$this->monto = 0;
		$this->TBK_RESPUESTA = -1;
		$this->authCode = "";
		$this->paymentTypeCode = "";
		$this->cuotas = "";
		$this->token = "";
		$this->finalTarjeta = "";
		$this->fecha = "";
	}

	function saveRespuesta($tbk_monto)
	{
		$sql = "update " . tablaRegistroTransacciones . " set TBK_ORDEN_COMPRA = '".$this->TBK_ORDEN_COMPRA."', ";
		$sql .= " TBK_RESPUESTA = ".$this->TBK_RESPUESTA.", TBK_MONTO = ".$tbk_monto.", ";
		$sql .= " TBK_CODIGO_AUTORIZACION = '".$this->authCode."', TBK_TIPO_PAGO = '".$this->paymentTypeCode."', ";
		$sql .= " TBK_NUMERO_CUOTAS = '".$this->cuotas."', TBK_FINAL_NUMERO_TARJETA = '".$this->finalTarjeta."' where idr = ".$this->id;

		mysqli_query($GLOBALS["conn"], $sql);
	}

	function loadToken($token)
	{
		$this->id = -1;
		$sql = "select idr, oc_carro, TBK_ORDEN_COMPRA, ifnull(TBK_RESPUESTA,-1), TBK_MONTO, TBK_CODIGO_AUTORIZACION, TBK_TIPO_PAGO, TBK_NUMERO_CUOTAS, TBK_FINAL_NUMERO_TARJETA, ";
		$sql .= " date_format(estampa,'%d-%m-%Y %H:%i'), total_carro ";
		$sql .= " from " . tablaRegistroTransacciones . " where token = '".$token."' ";

		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($d = mysqli_fetch_row($qry))
		{
			$this->id = $d[0];
			$this->OC = $d[1];
			$this->TBK_ORDEN_COMPRA = $d[2];
			$this->token = $token;
			$this->TBK_RESPUESTA = $d[3];
			$this->authCode = $d[5];
			$this->paymentTypeCode = $d[6];
			$this->cuotas = $d[7];
			$this->finalTarjeta = $d[8];
			$this->fecha = $d[9];
			$this->monto = $d[10];
		}
	}

	function loadOC($oc)
	{
		$this->id = -1;
		$sql = "select idr, token, TBK_ORDEN_COMPRA, TBK_RESPUESTA, TBK_MONTO, TBK_CODIGO_AUTORIZACION, TBK_TIPO_PAGO, TBK_NUMERO_CUOTAS, TBK_FINAL_NUMERO_TARJETA, ";
		$sql .= " date_format(estampa,'%d-%m-%Y %H:%i'), total_carro ";
		$sql .= " from " . tablaRegistroTransacciones . " where oc_carro = '".$oc."' ";

		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($d = mysqli_fetch_row($qry))
		{
			$this->id = $d[0];
			$this->token = $d[1];
			$this->TBK_ORDEN_COMPRA = $d[2];
			$this->OC = $oc;
			$this->TBK_RESPUESTA = $d[3];
			$this->authCode = $d[5];
			$this->paymentTypeCode = $d[6];
			$this->cuotas = $d[7];
			$this->finalTarjeta = $d[8];
			$this->fecha = $d[9];
			$this->monto = $d[10];
		}
	}

	function save()
	{
		$sql = "update " . tablaRegistroTransacciones . " set total_carro = ".$this->monto." where idr = ".$this->id;
		mysqli_query($GLOBALS["conn"], $sql);
	}

	function saveToken($token)
	{
		$sql = "update " . tablaRegistroTransacciones . " set token = '".$token."' where idr = ".$this->id;
		$this->token = $token;
		mysqli_query($GLOBALS["conn"], $sql);
	}

	function genOC()
	{
		$salida = "";
		$sql = "insert into " . tablaRegistroTransacciones . "(estampa) values (now())";
		mysqli_query($GLOBALS["conn"], $sql);
		$sql = "select max(idr) from " . tablaRegistroTransacciones . "";
		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($d = mysqli_fetch_row($qry))
		{
			$this->id = $d[0];
		}
		$salida = date("YmdHis"); 
		$conteo = 1;

		while ($this->validaOC($salida,$conteo))
		{
			$conteo++;
		}
		$temp = "".$conteo;
		while (strlen($temp) < 3)
		{
			$temp = "0".$temp;
		}
		$salida = $salida . $temp;




		$sql = "update " . tablaRegistroTransacciones . " set oc_carro = '".$salida."' where idr = ".$this->id;
		mysqli_query($GLOBALS["conn"], $sql);
		$this->OC = $salida;

		return $salida;
	}

	function validaOC($oc, $cont)
	{
		$salida = true;
		$temp = "".$cont;
		while (strlen($temp) < 3)
		{
			$temp = "0".$temp;
		}
		$temp = $oc.$temp;
		$sql = "select count(*) from " . tablaRegistroTransacciones . " where oc_carro = '".$temp."' ";
		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($d = mysqli_fetch_row($qry))
		{
			if ($d[0] == 0)
			{
				$salida = false;
			}
		}
		return $salida;
	}

	function getTipoCuota()
	{
		$salida = "";
		if (strtoupper($this->paymentTypeCode) == "VN")	{	$salida = "Sin Cuotas";	}
		if (strtoupper($this->paymentTypeCode) == "VC")	{	$salida = "Cuotas Normales";	}
		if (strtoupper($this->paymentTypeCode) == "SI")	{	$salida = "Sin Inter&eacute;s";	}
		if (strtoupper($this->paymentTypeCode) == "VD")	{	$salida = "Venta D&eacute;bito";	}
		if (strtoupper($this->paymentTypeCode) == "CI")	{	$salida = "Cuotas Normales";	}
		if (strtoupper($this->paymentTypeCode) == "NC")	{	$salida = "Sin Inter&eacute;s";	}
		if (strtoupper($this->paymentTypeCode) == "S2")	{	$salida = "Sin Inter&eacute;s";	}
		return $salida;		
	}

	function getTBK_TIPO_PAGO()
	{
		$salida = "";
		if (strtoupper($this->paymentTypeCode) == "VN")	{	$salida = "Cr&eacute;dito";	}
		if (strtoupper($this->paymentTypeCode) == "VC")	{	$salida = "Cr&eacute;dito";	}
		if (strtoupper($this->paymentTypeCode) == "SI")	{	$salida = "Cr&eacute;dito";	}
		if (strtoupper($this->paymentTypeCode) == "VD")	{	$salida = "Redcompra";	}
		if (strtoupper($this->paymentTypeCode) == "CI")	{	$salida = "Cr&eacute;dito";	}
		if (strtoupper($this->paymentTypeCode) == "NC")	{	$salida = "Cr&eacute;dito";	}
		if (strtoupper($this->paymentTypeCode) == "S2")	{	$salida = "Cr&eacute;dito";	}
		return $salida;
	}

	function notificaError()
	{
		$listaAndroid = array();
		$listaIOS = array();
		$sql = " select distinct u.ios, u.android from APP_BENEFICIO_PATENTE b, APP_USER_PATENTE upte, APP_USER u ";
		$sql .= " where b.OC = '".$this->OC."' and upte.patente = b.patente ";
		$sql .= " and upte.id_usuario = u.id ";


		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($qry) {
			while ($d = mysqli_fetch_row($qry)) {
				if (strlen(trim($d[0])) > 0)	{	array_push($listaIOS,$d[0]);	}
				if (strlen(trim($d[1])) > 0)	{	array_push($listaAndroid,$d[1]);	}
			}
		}

		for ($i = 0; $i < count($listaIOS); $i++)
		{
			$link = "http://www.seuvo.com/ws/api/api.php?NOTIFICA_IOS=".$listaIOS[$i]."&OC=".$this->OC."&ERROR=1";
			file_get_contents($link);
		}
		for ($i = 0; $i < count($listaAndroid); $i++)
		{
			$link = "http://www.seuvo.com/ws/api/api.php?NOTIFICA_ANDROID=".$listaAndroid[$i]."&OC=".$this->OC."&ERROR=1";
			file_get_contents($link);
		}
	}

	function notificaCierre()
	{
		$listaAndroid = array();
		$listaIOS = array();
		$sql = " select distinct u.ios, u.android from APP_BENEFICIO_PATENTE b, APP_USER_PATENTE upte, APP_USER u ";
		$sql .= " where b.OC = '".$this->OC."' and upte.patente = b.patente ";
		$sql .= " and upte.id_usuario = u.id ";


		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($qry) {
			while ($d = mysqli_fetch_row($qry)) {
				if (strlen(trim($d[0])) > 0)	{	array_push($listaIOS,$d[0]);	}
				if (strlen(trim($d[1])) > 0)	{	array_push($listaAndroid,$d[1]);	}
			}
		}

		for ($i = 0; $i < count($listaIOS); $i++)
		{
			$link = "http://www.seuvo.com/ws/api/api.php?NOTIFICA_IOS=".$listaIOS[$i]."&OC=".$this->OC;
			file_get_contents($link);
		}
		for ($i = 0; $i < count($listaAndroid); $i++)
		{
			$link = "http://www.seuvo.com/ws/api/api.php?NOTIFICA_ANDROID=".$listaAndroid[$i]."&OC=".$this->OC;
			file_get_contents($link);
		}
	}

	function notificaEmailCliente()
	{
		return true;
		$sql = "SELECT b.patente, m.meta_value FROM APP_BENEFICIO_PATENTE b, wp_cta_postmeta m ";
		$sql .= " where b.OC = '".$this->OC."' and b.id_post = m.post_id and m.meta_key = 'post_title' ";

		$patente = "";
		$titulo = "";
		$qry = mysqli_query($GLOBALS["conn"], $sql);
		if ($d = mysqli_fetch_row($qry))
		{
			$patente = $d[0];
			$titulo = $d[1];
		}


		$email = file_get_contents("/seuvo/notificacion_usuario.htm");

		$elMonto = str_replace(".","",$this->monto);
		$email = str_replace("[MONTO]", number_format($elMonto,0,".","."), $email);
		$email = str_replace("[MEDIO]","Pago Webpay", $email);
		$email = str_replace("[PATENTE]", $patente, $email);
		$email = str_replace("[PRODUCTO]", $titulo, $email);

		$sql = "select distinct u.email from APP_USER u, APP_USER_PATENTE p where u.id = p.id_usuario and p.patente = '".$patente."' and length(u.email) > 0";
		$qry = mysqli_query($GLOBALS["conn"], $sql);

		$mail = new PHPMailer(true);     
		$subject = "Notificacion de Compra Exitosa en Seuvo";
		$username = "contacto@seuvo.com";
		$password = "seuvo2016";
		$host = "smtp.zoho.com";
		$port = 465;

		while ($d = mysqli_fetch_row($qry))
		{
		    try {
		        
		        $mail->IsSMTP();
		        $mail->SMTPAuth = true;        
		        $mail->SMTPSecure = 'ssl';
		        $mail->Host = $host;
		        $mail->Port = $port;
		        $mail->Username = $username;  
		        $mail->Password = $password;
		        $mail->AddAddress($d[0] );
		        $mail->AddReplyTo('contacto@seuvo.com', 'Seuvo');
				$mail->SetFrom($username, 'Seuvo');
				$mail->Subject = $subject;
				$mail->Body = $email;
				$mail->IsHTML(true); 
				$mail->WordWrap = 70; 
				$mail->CharSet = 'UTF-8';
				$mail->Send();
		      
		    } catch (phpmailerException $e) {
		        ;
		    } catch (Exception $e) {
		        ;
		    }
		}

	}
}
?>
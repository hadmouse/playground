<?php
header ('Content-Type: application/json; charset=utf8');

require_once("../../../../../wp-load.php");

$GLOBALS["conn"] = conectar(true, true);
require_once("webpay.php");

$salida = array("status" => "error");
$monto = monto_reserva;

if (modo_test) {
	$monto = 15;
}

$webpay = new WebPay();
$webpay->genOC();
$webpay->monto = $monto;
$webpay->save();
$laOC = $webpay->OC;

$sql_check = mysqli_query($GLOBALS["conn"],  "SELECT * from " . tablaRegistroCompras . " WHERE OC = '$webpay->OC'");

if (mysqli_num_rows($sql_check) > 0) {
	$sql_update = "UPDATE " . tablaRegistroCompras . " SET estado = 0, fechaReserva = now(), monto= '$monto'  WHERE OC = '$webpay->OC'";
	mysqli_query($GLOBALS["conn"], $sql_update);
} else {
	$sql_insert = "INSERT INTO " . tablaRegistroCompras . " (estado, fechaReserva, OC, monto) VALUES(0, now(), '$webpay->OC', '$monto')";
	mysqli_query($GLOBALS["conn"], $sql_insert);
}

$params = array(
	"acc" => "INIT",
	"PROD" => 1,
	"SIS" => $webpay->OC,
	"RETURNURL" => redirectURL,
	"FINALURL" => finalURL,
	"AMOUNT" => $monto,
	"OC" => $webpay->OC
);

$link = wsWebpay . "?" . http_build_query($params);

$init = file_get_contents($link);
$jInit = json_decode($init);

$url = finalURL;
$token = "";

if ($jInit->status == "ok") {
	$token = $jInit->token;
	$url = $jInit->url;
	$webpay->saveToken($token);
} else	{
	$jInit->url = finalURL;
	$init = json_encode($jInit);
}

$token = $jInit->token;
$webpay->saveToken($token);
$_SESSION["oc"] = $webpay->OC;

echo $init;
mysqli_close($GLOBALS["conn"]);








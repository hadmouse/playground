<?php
header ('Content-Type: text/html; charset=utf8');

require_once("../../../../../wp-load.php");
require_once("phpmailer/PHPMailerAutoload.php");

$GLOBALS["conn"] = conectar(true, true);
require_once("webpay.php");

$token = $_REQUEST["token_ws"];
$webpay = new WebPay();
$webpay->loadToken($token);

$link = wsWebpay . "?acc=GETRESULT&PROD&token=" . $token;
$resultado = file_get_contents($link);

$jRes = json_decode($resultado);

if (strtoupper(trim($jRes->status)) == "OK") {
	$webpay->authCode = $jRes->authCode;
	$webpay->paymentTypeCode = $jRes->paymentTypeCode;
	$webpay->cuotas = $jRes->sharesNumber;
	$tbkMonto = $jRes->amount;
	$webpay->TBK_ORDEN_COMPRA = $jRes->OC;
	$webpay->finalTarjeta = $jRes->tarjeta;
	$webpay->TBK_RESPUESTA = $jRes->responseCode;
	$webpay->OC = $jRes->OC;
	$webpay->estado = $jRes->responseCode;
	$webpay->url = $jRes->url;
	$oc = intval($webpay->OC, 10);
	
	$url = finalURL;
	$hayError = true;
	if ($webpay->TBK_RESPUESTA == 0) {
		
		$estado = 1;
		$sql_check = "SELECT * from " . tablaRegistroCompras . " WHERE OC = $oc";
		$result = mysqli_query($GLOBALS["conn"], $sql_check);
		
		if (mysqli_num_rows($result) === 1) {
			while ($row = mysqli_fetch_array($result)) {
				$estado = intval($row["estado"], 10);
			}
		}

		if ($estado === 0) {
			// Confirmar pago
			$sql_update = "UPDATE " . tablaRegistroCompras . " SET estado = 1, fechaPago = now() WHERE OC = '" . $oc . "'";
			$result2 = mysqli_query($GLOBALS["conn"], $sql_update);
			
			$link = wsWebpay . "?acc=ACK&PROD&token=" . $token;
			file_get_contents($link);
			
			$webpay->notificaEmailCliente();
			$hayError = false;
			$url = $jRes->url;
			
			notificar($oc, $_SESSION["last_id"]);
		} else {
			$webpay->TBK_RESPUESTA = -1;
		}
	} else {
		$link = wsWebpay . "?acc=ACK&PROD&token=" . $token;
		file_get_contents($link);
	}
	$webpay->saveRespuesta($tbkMonto);

?>
	<form id='frmPost' action='<?php echo $url; ?>' method='post'>
		<input type='hidden' name='token_ws' value='<?php echo $token; ?>'>
	</form>
	<script>
		document.getElementById("frmPost").submit();
	</script>
<?php
	
} else {
	header("Location: " . finalURL);
	die();
}

mysqli_close($GLOBALS["conn"]);
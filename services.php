<?php
require("../../../wp-load.php");
header("Content-Type: application/json");


if (isset( $_REQUEST["require"] )) {
	$salida = array();
	$require = explode(" ", $_REQUEST["require"]);
	
	foreach ($require as $serv) {
		$retrieve = null;

		switch ($serv) {
			case "servicios":
				$retrieve = getServicios();
				break;
			case "banner":
			case "banners":
				$retrieve = getBanners();
				break;
		}
		
		$salida[$serv] = $retrieve;
	}

	die(json_encode($salida, JSON_PRETTY_PRINT));
}
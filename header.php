<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head(); ?>
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_home_url(); ?>/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_home_url(); ?>/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_home_url(); ?>/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_home_url(); ?>/favicon/site.webmanifest">
		<link rel="mask-icon" href="<?php echo get_home_url(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="<?php echo get_home_url(); ?>/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#00aba9">
		<meta name="msapplication-config" content="/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">
	</head>

	<body <?php body_class(); ?>>
		<div id="app">
			<header id="main-header" class="main-header">

				

			</header>
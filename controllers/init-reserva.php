<?php

require("../../../../wp-load.php");
header("Content-Type: application/json");

$data = json_decode(file_get_contents("php://input"), true);
$salida = array("status" => false);

if (
	isset($data["nombre"]) &&
	isset($data["fecha"]) &&
	isset($data["hora"]) &&
	isset($data["telefono"]) &&
	isset($data["servicio"])
) {

	$fecha = explode("-", $data["fecha"])[2] . "-" . explode("-", $data["fecha"])[1] . "-" . explode("-", $data["fecha"])[0];
	$data["fecha"] = $fecha;
	$data["patente"] = strtoupper($data["patente"]);
	
	$setReserva = setReserva($data);
	
	if ($setReserva !== false) {
		$salida["status"] = true;
		$salida["id"] = $setReserva;
		$_SESSION["last_id"] = $setReserva;
	}
}


die(json_encode($salida));
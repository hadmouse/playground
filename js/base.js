/*globals document, window, location, data, vs, XMLHttpRequest, Object, Date, setInterval, clearInterval*/
/*eslint-env es6*/
/*eslint no-unused-vars: off*/

var home_url = "/",
	dias = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
	meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	gen_methods = {
		basicAjax: function(opciones) {
			//opciones:
			//url, method, datatype, headers, data, callback, onerror

			let xhr = new XMLHttpRequest(),
				data = typeof opciones.data !== "undefined" ? opciones.data : {},
				method = typeof opciones.method !== "undefined" ? opciones.method : "POST",
				type = typeof opciones.datatype !== "undefined" ? opciones.datatype : "json";

			xhr.open(method, opciones.url, true);

			if (typeof opciones.headers === "object") {
				for (let key in opciones.headers) {
					if (opciones.headers.hasOwnProperty(key)) {
						xhr.setRequestHeader(key, opciones.headers[key]);
					}
				}
			} else {
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			}

			xhr.responseType = type;

			if (typeof opciones.onerror === "function") {
				xhr.onerror = opciones.onerror;
			}

			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4 && xhr.status === 200) {
					if (typeof opciones.callback === "function") {
						opciones.callback(xhr.response);
					}
				}
			};

			xhr.send(gen_methods.jsonToQueryString(data));
		},
		calcularEdad: function(aaaa_mm_dd) {
			var fecha_nac = new Date(aaaa_mm_dd),
				hoy = new Date(),
				un_anno = 1000 * 60 * 60 * 24 * 365;

			//fix timezone
			fecha_nac.setTime(fecha_nac.getTime() + (fecha_nac.getTimezoneOffset() * 60 * 1000));

			return Math.floor( (hoy.getTime() - fecha_nac.getTime()) / un_anno );
		},
		generarForm: function (object, method) {
			var formulario = document.createElement("form"),
				temp;

			formulario.setAttribute("method", typeof method !== "undefined" ? method : "post");

			for (let key in object) {
				if (object.hasOwnProperty(key)) {
					if (key === "url" || key === "action") {
						formulario.setAttribute("action", object[key]);
					} else {
						temp = document.createElement("input");
						temp.setAttribute("name", key === "token" ? "token_ws" : key);
						temp.setAttribute("value", object[key]);
						formulario.appendChild(temp);
					}
				}
			}

			document.body.appendChild(formulario);
			formulario.submit();
		},
		getSrc: function (src) {
			return src.full.src;
		},
		getSrcset: function (src) {
			let output = [];
			for (let key in src) {
				if (src.hasOwnProperty(key)) {
					output.push(src[key].src + " " + src[key].width + "w");
				}
			}
			return output.join();
		},
		getVS: function () {
			if (typeof vs !== "undefined") {
				return vs;
			}
			return null;
		},
		jsonToQueryString: function (json) {
			return Object.keys(json).map( function (key) {
				return encodeURIComponent(key) + '=' + encodeURIComponent(json[key]);
			}).join('&');
		},
		queryToJson: function () {
			var search = location.search.substring(1);
			return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
		},
		customQueryStringToJson: function (string) {
			var search = string
			return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
		},
		placeholder: function (just_hide) {
			var ocultar = typeof just_hide === "boolean" ? just_hide : false;
			if (ocultar === true) {
				return "this.style.display = 'none'";
			}
			return "this.setAttribute('src', '" + vs.theme_location + "/assets/productos/placeholder.png')";
		},
		parseQueryString: function() {
			return JSON.parse('{"' + decodeURIComponent(location.search).substr(1).replace(/\=/g, '":"').replace(/\&/g, '", "') + '"}');
		},
		smoothScroll: function(element, margin_top) {
			let fps = 13,
				mt = margin_top !== undefined ? margin_top : 0,
				actual = window.scrollY,
				distancia = element.getBoundingClientRect().top - mt,
				direccion = distancia > 0 ? "abajo" : "arriba",
				contador_pasos = 0,
				pasos = 40,
				sobrante = distancia % pasos,
				ease = function(distancia, pasos) {
					var salida = [],
						total = distancia;

					for (let i = 0; i < pasos; i += 1) {
						let este = (total / pasos);

						if (pasos % 2 !== 0 && i === Math.floor(pasos / 2)) {
							este = este * 2;
						} else if (i < pasos / 2) {
							este = este * (((i + 1) * 100 / pasos) / 100) * 4;
						} else {
							este = este * ((100 - ((i + 1) * 100 / pasos)) / 100) * 4;
						}
						salida.push(este);
					}

					return salida;
				},
				distancia_por_paso = ease(distancia, pasos);

			let intervalo = setInterval(function () {
				let espacio = Math.round(window.scrollY + distancia_por_paso[contador_pasos]);
				window.scrollTo(0, espacio);

				if (contador_pasos === (pasos - 1)) {
					clearInterval(intervalo);
				}

				contador_pasos += 1;
			}, fps);
		},
		toggleModalOpened: function () {
			if (document.body.className.indexOf(" modal-open") === -1) {
				document.body.className = document.body.className + " modal-open";
			} else {
				document.body.className = document.body.className.replace(" modal-open", "");
			}
		}
	},
	filtros = {
		noSpaces: function (value) {
			return value.replace(/\ /g, "");
		},
		onlyNumbers: function (value, addicionalesAdmitidos) {
			let allowed = addicionalesAdmitidos !== undefined ? "0123456789" + addicionalesAdmitidos : "0123456789",
				salida = value.split("").filter(function (v) {
					return allowed.indexOf(v) !== -1;
				});
			return salida.join("");
		},
		numero: function (value) {
			if (typeof value === "undefined") {
				return "";
			}

			return value.toString().split("").reverse().map(function (v, i) {
				return i % 3 === 0 && i !== 0 ? v + "." : v;
			}).reverse().join("");
		},
		capitalize: function (valor, procesar) {
			let vv = valor !== undefined ? valor : "",
				v = vv.toLowerCase().split(" ");

			v.forEach(function (value, index) {
				if (value.substr(0, 1) === "¡" || value.substr(0, 1) === "¿") {
					v[index] = value.substr(0, 2).toUpperCase() + value.substr(2);
				} else {
					v[index] = value.substr(0, 1).toUpperCase() + value.substr(1);
				}
			});
			return v.join(" ");
		},
		espaciado: function (value) {
			var i = 0,
				salida = "";

			while (i < value.length) {
				if (value.substr(i, 4).length < 2) {
					salida = salida.trim();
				}
				salida += value.substr(i, 4) + " ";
				i += 4;
			}
			
			return salida.trim();
		},
		fecha: function (fecha, arrayFormato, hoy) {
			var h = typeof hoy === "boolean" ? hoy : false,
				salida = "",
				objFecha = new Date(fecha);
			
			if (h) {
				h = objFecha.toDateString() === new Date().toDateString();
				if (h) {
					salida = "Hoy, ";
				}
			}
			
			if (typeof arrayFormato !== "object" || objFecha == "Invalid Date") {
				return null;
			}
			
			arrayFormato.forEach(function (v) {
				let vv = v.toLowerCase(),
					este = "";
				
				switch (vv) {
					case "u":
					case "unix":
					case "milisegundos":
					case "miliseconds":
					case "time":
					case "tiempo":
						este = objFecha.getTime();
						break;
					case "s":
					case "segundo":
					case "segundos":
					case "second":
					case "seconds":
						este = objFecha.getSeconds();
						este = este < 10 ? "0" + este : este;
						break;
					case "min":
					case "minuto":
					case "minutos":
					case "minute":
					case "minutes":
						este = objFecha.getMinutes();
						este = este < 10 ? "0" + este : este;
						break;
					case "h":
					case "hora":
					case "horas":
					case "hour":
					case "hours":
						este = objFecha.getHours();
						break;
					case "d":
					case "dia":
					case "día":
						if (!h) {
							este = objFecha.getDate();
							este = este < 10 ? "0" + este : este;
						}
						break;
					case "dia-string":
					case "diastring":
					case "daystring":
					case "day-string":
					case "stringday":
					case "string-day":
						if (!h) {
							este = dias[objFecha.getDay()];
						}
						break;
					case "m":
					case "mes":
					case "month":
						if (!h) {
							este = objFecha.getMonth() + 1;
							este = este < 10 ? "0" + este : este;
						}
						break;
					case "messtring":
					case "mes-string":
					case "monthstring":
					case "month-string":
					case "stringmonth":
					case "string-month":
						if (!h) {
							este = meses[objFecha.getMonth()];
						}
						break;
					case "a":
					case "y":
					case "año":
					case "ano":
					case "anno":
					case "year":
					case "full-year":
					case "fullyear":
						if (!h) {
							este = objFecha.getFullYear();
						}
						break;
					case "añocorto":
					case "año-corto":
					case "anocorto":
					case "ano-corto":
					case "annocorto":
					case "anno-corto":
					case "shortyear":
					case "short-year":
						if (!h) {
							este = objFecha.getFullYear().toString().substr(-2, 2);
						}
						break;
					case ",":
					case "coma":
					case "comma":
						if (!h) {
							este = ", ";
						}
						break;
					case "slash":
					case "barra":
					case "/":
						if (!h) {
							este = "/";
						}
						break;
					case ":":
					case "dospuntos":
					case "dos-puntos":
						este = ":";
						break;
					case "guíon":
					case "guion":
					case "hyphen":
					case "-":
					case "_":
						if (!h) {
							este = "-";
						}
						break;
					default:
						este = " ";
				}
				
				salida += este;
			});
			
			return salida;
		}
	};
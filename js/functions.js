/*globals document, window, alert, data, Vue, vs, setInterval, setTimeout, clearInterval, location*/
/*eslint no-unused-vars: off*/
/*eslint-env es6*/

//añadir clase al body al bajar en scroll
var defer_threshold = 3,
	defer_count = 0,
	section_tienda = document.getElementById("section-tienda");

window.addEventListener("scroll", function () {
	defer_count += 1;

	if (defer_count % defer_threshold === 0) {
		let threshold = window.outerHeight / 2,
			current_top = window.scrollY;
		
		if (current_top > threshold) {
			if (document.body.className.indexOf("scrolled") === -1) {
				document.body.className += " scrolled";
			}
		} else {
			document.body.className = document.body.className.replace(" scrolled", "");
		}
	}
}, { passive: true });

//smooth scroll
function ease(distancia, pasos) {
    var salida = [],
        total = distancia;

    for (let i = 0; i < pasos; i += 1) {
        let este = (total / pasos);

        if (pasos % 2 !== 0 && i === Math.floor(pasos / 2)) {
            este = este * 2;
        } else if (i < pasos / 2) {
            este = este * (((i + 1) * 100 / pasos) / 100) * 4;
        } else {
            este = este * ((100 - ((i + 1) * 100 / pasos)) / 100) * 4;
        }
        salida.push(este);
    }

    return salida;
}

function smooth_scroll(element, margin_top) {
	let fps = 13,
		mt = margin_top !== undefined ? margin_top : 0,
		actual = window.scrollY,
		distancia = element.getBoundingClientRect().top - mt,
		direccion = distancia > 0 ? "abajo" : "arriba",
		contador_pasos = 0,
		pasos = 40,
		sobrante = distancia % pasos,
		distancia_por_paso = ease(distancia, pasos);
	
	let intervalo = setInterval(function () {
		let espacio = Math.round(window.scrollY + distancia_por_paso[contador_pasos]);
		window.scrollTo(0, espacio);
		
		if (contador_pasos === (pasos - 1)) {
			clearInterval(intervalo);
		}
		
		contador_pasos += 1;
	}, fps);
}

function check_ease(array_pasos) {
	var b = 0,
        c = array_pasos.map(v => b += v);
	return b;
}

function addClass(el, the_class) {
	let classes = el.getAttribute("class") !== null ? el.getAttribute("class").split(" ") : [];
	
	if (classes.indexOf(the_class) === -1) {
		classes.push(the_class);
		el.setAttribute("class", classes.join(" "));
	}
}

function removeClass(el, the_class) {
	let classes = el.getAttribute("class") !== null ? el.getAttribute("class").split(" ") : [];
	
	if (classes.indexOf(the_class) !== -1) {
		while (classes.indexOf(the_class) !== -1) {
			classes.splice(classes.indexOf(the_class), 1);
		}
		el.setAttribute("class", classes.join(" "));
	}
}

function igualarAlto(el) {
	el.style.height = el.clientWidth + "px";
}

function offset(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function fixFaqFlexbox() {
	var w = window.outerWidth > 0 ? window.outerWidth : window.innerWidth;

	if (w > 767 && w < 1201) {
		var ul = document.querySelector("ul.faq"),
			li = ul.querySelectorAll("li"),
			i = 0,
			h = 0;

		for (i; i < li.length; i += 1) {
			//if (i === 0 || i === 3 || i === 6) {
			if (i === 0 || i === 3) {
				h+= li[i].clientHeight;
			}
		}

		ul.style.height = h + "px";
	}
}

fixFaqFlexbox();

var enlaces_header = document.querySelectorAll("a[href*='#']"),
	igual_alto = document.querySelectorAll(".igualar-alto-a-ancho");

for (let key in enlaces_header) {
	if (enlaces_header.hasOwnProperty(key)) {
		enlaces_header[key].addEventListener("click", function (event) {
			event.preventDefault();
			let target = document.querySelector("a[name='" + this.getAttribute("href").split("#")[1] + "']");
			if (target !== null) {
				smooth_scroll(target);
			} else {
				location.href = this.getAttribute("href");
			}
		});
	}
}

for (let key in igual_alto) {
	if (igual_alto.hasOwnProperty(key)) {
		igualarAlto(igual_alto[key]);
	}
}
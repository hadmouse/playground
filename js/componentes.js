/*globals document, Vue, gen_methods, filtros, console, alert, location, setTimeout, data_registros, window, Bus*/
/*eslint-env es6*/
/*eslint no-unused-vars: off*/
/*eslint no-console: off*/

Vue.component("tooltip", {
	props: {
		texto: String
	},
	template: `<div class="tooltip" ` + (window.outerWidth < 768 ? `@click="active = !active"` : `@mouseenter="active = true" @mouseleave="active = false"`) + ` :class="{ 'active': active }">
					<span class="tooltip_icon">i</span>
					<div class="tooltip_content">
						<p>{{ texto }}</p>
					</div>
				</div>`,
	methods: {},
	data: function () {
		return {
			active: false
		}
	}
});

Vue.component("paso", {
	props: {
		indice: Number,
		imagen: "",
		texto: "",
		classes: {
			default: false
		},
		doble: {
			default: false
		}
	},
	template: `<article class='paso' :class='setClasses(classes)'>
					<div class='paso_inner'>
						<div class='paso_imagen-wrapper' v-if='imagen'>
							<img :src='img(imagen)'>
						</div>
						<div class='paso_texto' :data-counter="getCounter(indice)" v-if='!doble'>
							<p>{{texto}}</p>
						</div>
						
						<div class='paso_texto' :data-counter="getCounter(indice)" v-else>
							<h3>Caso A:</h3>
							<p v-html='texto[0]'></p>
							<hr>
							<h3>Caso B:</h3>
							<p v-html='texto[1]'></p>
						</div>
					</div>
				</article>`,
	methods: {
		img: function (url) {
			return "/wp-content/themes/playground/assets/svg/" + url;
		},
		setClasses: function (classes) {
			if (classes !== false) {
				return classes;
			}
			return "";
		},
		getCounter: function (indice) {
			return indice < 2 ? indice + 1 : indice;
		}
	}
});

Vue.component("gallery-item", {
	props: {
		"src": {},
		"title": String,
		"subtitle": String,
		"content": String,
		"id": {},
		"resolvesrc": {
			default: true
		}
	},
	template: `<article class='gallery-item' :id='id'>
		<img :srcset='resolvesrc ? getSrcset(src) : src' :src='resolvesrc ? getSrc(src) : src'>
		<div class='gallery-item_inner'>
			<h2 v-if='title' v-html='title'></h2>
			<h3 v-if='subtitle'>{{subtitle}}</h3>
			<div class='gallery-item_content' v-if='content' v-html='content'></div>
		</div>
	</article>`,
	methods: {
		getSrcset: gen_methods.getSrcset,
		getSrc: gen_methods.getSrc
	}
});

Vue.component("gallery-bullet", {
	props: ["id"],
	template: `<li class='gallery-bullet'><button @click='clicked(id)'></button></li>`,
	methods: {
		clicked: function (goto) {
			this.$emit("clicked", goto);
		}
	}
});

Vue.component("post-gallery", {
	props: { 
		active: "", 
		indexes: "",
		flechas: {
			default: function () {
				return ["icon_flecha-izquierda.svg", "icon_flecha-derecha.svg"];
			}
		},
		useNumber: {
			default: false
		}
	},
	template: `<section class='post-gallery'>
			<button class='prev' @click='prev()'><img :src=' getVS().theme_location + "/assets/svg/" + flechas[0] '></button>
				<div class='post-gallery_inner'>
					<slot></slot>
				</div>
			<button class='next' @click='next()'><img :src=' getVS().theme_location + "/assets/svg/" + flechas[1] '></button>
			<div v-if="useNumber" class="post-gallery_current">
				{{computedNumber}}
			</div>
	</section>`,
	methods: {
		next: function () {
			this.$emit("next");
		},
		prev: function () {
			this.$emit("prev");
		},
		getVS: gen_methods.getVS
	},
	computed: {
		computedNumber: function () {
			let active = parseInt(this.active, 10) + 1;
			return (active * 2 - 1) + " - " + (active * 2);
		}
	}
});

Vue.component("modal", {
	props: {
		titulo: "",
		usar_titulo: {
			default: true
		},
		usar_footer: {
			default: true
		}
	},
	template: `<article class="modal">
		<div class="modal_wrapper">
			<div class="modal_inner">
				<button class='btn-modal-close' @click='$emit("close"); toggleModalOpened()'>
					<img :src="getVS().theme_location + '/assets/svg/cerrar_negro.svg'" alt="cerrar">
				</button>
				<h2 v-if="usar_titulo">{{titulo}}</h2>
				<slot></slot>
				<footer class="modal_footer" v-if="usar_footer"><slot name="footer"></slot></footer>
			</div>
		</div>
	</article>`,
	methods: {
		getVS: gen_methods.getVS,
		toggleModalOpened: gen_methods.toggleModalOpened
	}
})

Vue.component("form-group", {
	props: {
		name: {
			default: ""
		},
		llave: {
			default: ""
		},
		value: {
			default: "",
			required: true
		},
		tipo: {
			default: "input",
			type: String
		},
		addon: {
			default: false
		},
		addon_position: {
			default: "back"
		},
		placeholder: {
			default: ""
		},
		opciones: {
			default: null
		},
		maxlength: {
			default: 50,
			type: Number
		},
		alias: {
			default: false
		},
		capitalizar: {
			default: true
		},
		hiddenLabel: {
			default: false
		},
		classes: {
			default: "",
			type: String
		},
		readonly: {
			default: false,
			type: Boolean
		},
		autocomplete: {
			default: false
		},
		status: {
			default: ""
		},
		min: false,
		max: false,
		step: false
	},
	template: `<div class="form-group" :class="classes + (active ? ' active' : '')">
			
				<p v-if="tipo === 'radio' || (tipo === 'checkbox' && opciones !== null)" class="control-label" style="margin: 0;">
					<span v-if="alias !== false">{{alias | capitalize(capitalizar) }}</span>
					<span v-else>{{name | capitalize(capitalizar) }}</span>
					<slot name="tooltip"></slot>
					<slot name="sub"></slot>
				</p>
				<label v-else :for="named" class="control-label" :class="{ 'sr-only': hiddenLabel }">
					<span v-if="alias !== false">{{alias | capitalize(capitalizar) }}</span>
					<span v-else>{{name | capitalize(capitalizar) }}</span>
					<slot name="tooltip"></slot>
					<slot name="sub"></slot>
				</label>

				<div v-if="tipo !== 'select' && tipo !== 'textarea'" class="input-group" :class="{ 'form-input-group': addon !== false }">
					<div v-if="addon !== false && addon_position === 'front' && tipo !== 'checkbox'" class="form-group-addon form-group-addon--before">{{ addon }}</div>

					<input  v-if="tipo !== 'checkbox' && tipo !== 'radio'"
							:type="tipo === 'input' ? 'text' : tipo"
							class="form-control"
							:class="status"
							:id="named"
							:name="named"
							:maxlength="maxlength"
							:placeholder="placeholder"
							:value="value"
							:readonly="readonly"
							:disabled="readonly"
							:min="min"
							:max="max"
							:step="step"
							:autocomplete="autocomplete"
							@focus="moveLabel()"
							@input="updateValue($event.target.value)"
							@blur="enviarAValidar($event.target.value);">

					<span v-if="addon !== false &&  tipo === 'checkbox' && opciones === null" style="margin-right: 5px;display: inline-block;float: left;margin-top: 8px;">{{ addon }}</span>
					<input  v-if="tipo === 'checkbox' && opciones === null"
							:class="status"
							:type="tipo"
							:id="named"
							:name="named"
							:readonly="readonly"
							:disabled="readonly"
							:checked="value"
							style="margin-top: 12px !important;"
							@change="updateValueBool">

					<label v-if="tipo === 'checkbox' && opciones !== null" v-for="opcion in opciones" :for="named + '-' + sanitize(opcion)" style="display: block; padding: 5px 0;">
						<input :class="status" type="checkbox" :name="named + '[]'" :id="named + '-' + sanitize(opcion)" :value="opcion" @click="updateValueCheckboxes($event.target)" style="margin-right: 5px !important;" v-model="checked_array">
						{{opcion}}
					</label>
				
					<label v-if="tipo === 'radio'" v-for="opcion in opciones" :for="named + '-' + sanitize(opcion)" style="display: block; padding: 5px 0;">
						<input :class="status" type="radio" :name="named" :id="named + '-' + sanitize(opcion)" :value="opcion" @change="updateValue($event.target.value)" style="margin-right: 5px !important;">
						{{opcion}}
					</label>

					<div v-if="addon !== false && addon_position === 'back' && tipo !== 'checkbox'" class="form-group-addon form-group-addon--after">{{ addon }}</div>
				</div>

				<div v-if="tipo === 'textarea'" class="input-group" :class="{ 'form-input-group': addon !== false }">
					<div v-if="addon !== false && addon_position === 'front'" class="form-group-addon form-group-addon--before">{{ addon }}</div>

					<textarea class="form-control"
							:class="status"
							:id="named"
							:name="named"
							:maxlength="maxlength"
							:placeholder="placeholder"
							:value="value"
							:readonly="readonly"
							:disabled="readonly"
							:autocomplete="autocomplete"
							@focus="moveLabel()"
							@input="updateValue($event.target.value)"
							@blur="enviarAValidar($event.target.value)"></textarea>

					<div v-if="addon !== false && addon_position === 'back'" class="form-group-addon form-group-addon--after">{{ addon }}</div>
				</div>

				<div v-if="tipo === 'select'" class="input-group">
					<select
						:id="named"
						:name="named"
						class="form-control"
						:class="status"
						@input="updateValue($event.target.value)"
						@blur="enviarAValidar($event.target.value)">
							<option disabled selected>- Seleccione -</option>
							<option v-for="opcion in opciones" :value="opcion.valor">{{ opcion.label }}</option>
					</select>
				</div>

				<div v-if="con_mensaje" class="mensaje-error">
					<p>{{mensaje}}</p>
				</div>
	   </div>`,
	filters: {
		capitalize: filtros.capitalize
	},
	methods: {
		updateValue: function (valor) {
			this.$emit("input", valor);
		},
		updateValueBool: function () {
			this.checked = !this.checked;
			this.$emit("input", this.checked);
		},
		updateValueCheckboxes: function (target) {
			if (target.checked && this.checked_array.indexOf(target.value) === -1) {
				this.checked_array.push(target.value);
			} else if (!target.checked && this.checked_array.indexOf(target.value) >= 0) {
				this.checked_array.splice(this.checked_array.indexOf(target.value), 1);
			}
			this.$emit("input", this.checked_array);
		},
		enviarAValidar: function (valor) {
			this.$emit("blur", valor, this.llave, this);
			if (valor === "") {
				this.active = false;
			}
		},
		moveLabel: function () {
			this.active = true;
		},
		tieneError: function (mensaje) {
			this.con_mensaje = true;
			this.mensaje = mensaje;
		},
		noTieneError: function () {
			this.con_mensaje = false;
			this.mensaje = "";
		},
		sanitize: function (v) {
			return v.toLowerCase().replace(/\./g, "-").replace(/\-/g, "-").replace(/ /g, "-").replace(/\ñ/g, "nm").replace(/á/g, "a").replace(/é/g, "e").replace(/í/g, "i").replace(/ó/g, "o").replace(/ú/g, "u");
		},
	},
	data() {
		return {
			active: false,
			con_mensaje: false,
			mensaje: "",
			checked: this.value ? true : false,
			checked_array: Array.isArray(this.value) ? this.value : [],
			named: this.llave
		}
	},
	mounted: function () {
		var t = this;
		//si es checkbox multiple y tiene valores por defecto del array, validarlos contra las opciones que trae
		if (this.tipo === "checkbox" && Array.isArray(this.opciones)) {
			if (typeof this.value === "string") {
				if (this.opciones.indexOf(this.value) >= 0) {
					this.checked_array.push(this.value);
				}
				this.$emit("input", this.checked_array);
			}
		}
	}
});

Vue.component("simulador", {
	props: {
		form: {
			type: Object,
			required: true
		},
		blurear: {
			type: Function,
			requred: true
		},
		enviar: {
			type: Function,
			requred: true
		},
		clasecargando: {
			type: Boolean,
			required: true
		},
		mostrarBoton: {
			type: Boolean,
			default: true,
			required: false
		}
	},
	template: `<form class="form-simulador" id="form-simulador">
					<form-group v-for="(input, key) in form"
						v-model="input.value"
						@blur="blurear"
						:key="key"
						:llave="key"
						:tipo="input.tipo"
						:placeholder="input.placeholder"
						:maxlength="input.maxlength"
						:min="input.min"
						:max="input.max"
						:step="input.step"
						:classes="input.classes"
						:addon="input.addon"
						:readonly="input.readonly"
						:posicion_tooltip="input.posicion_tooltip"
						:opciones="input.opciones"
						:alias="input.label">
						<template slot="tooltip" v-if="input.tooltip && input.posicion_tooltip === 'tooltip'">
							<tooltip :texto="input.tooltip"></tooltip>
						</template>
						<template slot="sub" v-if="input.posicion_tooltip === 'sub'">
							<sub>{{input.tooltip}}</sub>
						</template>
					</form-group>
					<div class="text-center sm-text-right" v-if="mostrarBoton">
						<button class="btn btn-color-1 btn-block sm-btn-inline-block" @click.prevent="enviar" :class="{ 'btn-cargando': clasecargando, 'btn-disabled': clasecargando }">Enviar</button>
					</div>
					<p class="disclaimer sub text-center sm-text-left">Valores referenciales, en ningún caso constituyen una cotización formal o exacta.</p>
				</form>`
});

Vue.component("resultado-simulacion", {
	props: {
		respuesta: {
			required: true
		},
		textoCerrar: {
			type: String,
			default: "Volver"
		},
		procesarDatos: {
			type: Function,
			required: false
		}
	},
	template: `<div class="simulacion-resultado" :class="{ 'active': respuesta !== null }">
					<div class="simulacion-resultado_inner">
						<h4>Resultado</h4>
						<p>A continuación el resultado de tu simulación:</p>
						<ul class="lista-respuesta">
							<li class="clear-after respuesta-cuota">
								<div class="sm-float-6 label">
									<p>Cuota:</p>
								</div>
								<div class="sm-float-6 value">
									<p v-if="respuesta !== null">$ {{ respuesta.Cuota | numero }}</p>
								</div>
							</li>
							<li class="clear-after">
								<div class="sm-float-12 label text-center">
									<p>
										Valor cuota es sólo referencial, en ningún caso constituye una cotización formal.
										<br><br>
										Incluye valor de la vivienda, seguro de desgravamen, impuestos y gastos administrativos.
									</p>
								</div>
							</li>
						</ul>
						<div class="botones text-center sm-text-right">
							<slot></slot>
							<button class="btn btn-ghost btn-block sm-btn-inline-block" @click.prevent="cerrar()">{{textoCerrar}}</button>
						</div>
					</div>
				</div>`,
	filters: {
		numero: filtros.numero
	},
	methods: {
		cerrar: function () {
			if (this.procesarDatos !== undefined) {
				this.procesarDatos();
			}
			this.$emit("cerrar");
			this.toggleModalOpened();
		},
		toggleModalOpened: gen_methods.toggleModalOpened
	}
});

//admin only
Vue.component("simulaciones", {
	props: {
		data: {
			type: Array
		}
	},
	template: `<table class="tabla tabla--anidada">
					<thead>
						<tr>
							<th>Precio Vivienda</th>
							<th>Aporte Particular</th>
							<th>Porcentaje Solicitado</th>
							<th>Plazo Meses</th>
							<th>Edad</th>
							<th>Porcentaje hipotecario</th>
							<th>Años hipotecario</th>
							<th>Valor cuota</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="entry in data">
							<td data-th="Precio Vivienda">			{{ entry.precio_venta_vivienda | numero }} UF											</td>
							<td data-th="Aporte Particular">		{{ entry.porcentaje_aporte_particular | numero }}%										</td>
							<td data-th="Porcentaje Solicitado">	{{ entry.porcentaje_solicitado | numero }}%												</td>
							<td data-th="Plazo Meses">				{{ entry.meses_propie | numero }}														</td>
							<td data-th="Edad">						{{ entry.edad }}																		</td>
							<td data-th="Porcentaje hipotecario">	{{ entry.porcentaje_hipotecario | numero }}%											</td>
							<td data-th="Años hipotecario">			{{ entry.annos_hipotecario | numero }}													</td>
							<td data-th="Valor cuota">$				{{ entry.valor_cuota | numero }}														</td>
							<td data-th="Fecha">					{{ entry.timestamp | fecha(["d", "-", "m", "-", "a", " ", "h", ":", "min"], true) }}hrs </td>
						</tr>
					</tbody>
				</table>`,
	filters: {
		numero: filtros.numero,
		fecha: filtros.fecha
	}
});

/*
Nombre
Email
Rut
Sexo
Fecha nacimiento
Renta
Teléfono
Fecha
Hora
Precio vivienda
Campaña
Simuló

<th class="th-cursor">Sexo</th>
<th class="th-cursor">Fecha nacimiento</th>
<th class="th-cursor">Precio vivienda</th>
*/

Vue.component("fila-registro", {
	props: {
		data: {
			required: true,
			type: Object
		},
		mostrarUrl: {
			type: Boolean,
			default: true
		},
		mostrarCampana: {
			type: Boolean,
			default: true
		},
		anchoAmpliado: {
			type: Number,
			default: 8
		},
		fusionarUrlCampana: {
			type: Boolean,
			default: false
		}
	},
	template: `<tr class="fila-registro" :class="{ 'active': ampliado, 'destacado': destacado }">
					<td :class="{ 'hidden': ampliado }" data-th="Nombre">{{ data.nombre | capitalize }}</td>
					<td :class="{ 'hidden': ampliado }" data-th="Rut">{{ data.rut | rut }}</td>
					<td :class="{ 'hidden': ampliado }" data-th="Email"> <a :href="'mailto:' + data.email">{{ data.email }}</a> </td>
					<td :class="{ 'hidden': ampliado }" data-th="Renta">{{ data.renta }}</td>
					<td :class="{ 'hidden': ampliado }" data-th="Fecha">{{ data.timestamp | fecha(["d", "-", "m", "-", "a"], false) }} </td>
					<td :class="{ 'hidden': ampliado }" data-th="Fecha">{{ data.timestamp | fecha(["h", ":", "min"], false) }} </td>
					<td :class="{ 'hidden': ampliado }" data-th="Sexo">{{ data.sexo }}</td>
					<td :class="{ 'hidden': ampliado }" data-th="Fecha Nacimiento">{{ data.fecha_nacimiento }}</td>
					<td :class="{ 'hidden': ampliado }" data-th="Teléfono">  <a :href="'tel:' + data.telefono">{{ data.telefono }}</a> </td>
					<td :class="{ 'hidden': ampliado }" data-th="Precio vivienda">{{ data.valor_vivienda }}</td>
					<td v-if="mostrarUrl && !fusionarUrlCampana" :class="{ 'hidden': ampliado }" data-th="Url Origen">{{ data.url_origen !== "" ? data.url_origen.replace('http://www.propie.cl', '') : "(unset)" }}</td>
					<td v-if="mostrarCampana && !fusionarUrlCampana" :class="{ 'hidden': ampliado }" data-th="Campaña">{{ data.campana }}</td>
					<td v-if="fusionarUrlCampana" :class="{ 'hidden': ampliado }" data-th="Campaña" v-html="fusionUrlCampana()"></td>

					<td :colspan="ampliado ? anchoAmpliado : 1" data-th="Simuló" :class="{ 'loading': cargando }">
						<simulaciones v-if="simulaciones !== null" :data="simulaciones" />
						<div class="footer" :class="{ 'active': ampliado }">
							<button v-if="data.simulo" class='btn btn-xs btn-color-1' @click="buscarSimulaciones(rut, id)">
								<span v-show="!ampliado">Sí, revisar</span>
								<span v-show="ampliado">Listo</span>
							</button>
							<span v-else>No</span>
						</div>
					</td>
				</tr>`,
	methods: {
		buscarSimulaciones: function (rut, id) {
			if (!this.ampliado) {
				this.resetearOtros();
				this.ampliado = !this.ampliado;
				this.cargando = true;
				this.$emit("seleccionado", this, rut, id);
			} else {
				this.ampliado = false;
				this.simulaciones = null;
				this.$emit("vaciado");
			}
		},
		resetearOtros: function () {
			Bus.$emit("resetear", this.id);
		},
		fusionUrlCampana: function () {
			if (this.data.campana === "") {
				return this.data.url_origen.replace('http://www.propie.cl', '') + "_mailing";
			} else {
				return this.data.url_origen.replace('http://www.propie.cl', '') + "_" + this.data.campana;
			}
		}
	},
	filters: {
		capitalize: filtros.capitalize,
		rut: function (rut) {
			var value, dv;
			if (typeof rut === "undefined") {
				return "";
			}
			
			value = rut.split("-")[0];
			dv = rut.split("-")[1];

			value = value.toString().split("").reverse().map(function (v, i) {
				return i % 3 === 0 && i !== 0 ? v + "." : v;
			}).reverse().join("");
			
			return value + "-" + dv;
		},
		fecha: filtros.fecha
	},
	data: function () {
		return {
			ampliado: false,
			cargando: false,
			simulaciones: null,
			destacado: false
		}
	},
	mounted: function () {
		var t = this;
		Bus.$on("resetear", function (id) {
			if (t.id !== id) {
				t.simulaciones = null;
				t.ampliado = false;
				t.cargando = false;
			}
		});
		Bus.$on("destacar", function (rut) {
			if (t.rut === rut) {
				t.destacado = true;
				setTimeout(function () {
					gen_methods.smoothScroll(document.querySelector("tr.fila-registro.destacado"));
				}, 15);
				setTimeout(function () {
					t.destacado = false;
				}, 4000);
			}
		});
	}
});


















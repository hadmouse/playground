/*globals window, document, location, localStorage, sessionStorage, alert*/
/*globals Vue*/
/*globals gen_methods, filtros*/

/*eslint-env es6*/
/*eslint no-unused-vars: off*/
/*eslint no-console: off*/
var app = new Vue({
	el: "#app",
	data: {},
	methods: {
		//forms region
		validar: function (tipo, valor) {
			var rut = function (valor) {
					let v = valor.replace('-', '').replace(/\./g, '').toUpperCase(),
						sum = 0,
						i = 0,
						dvr = "0",
						mul = 2,
						u = v.split("").filter(function (vv) {
							return !isNaN(parseInt(vv, 10)) || vv === "K";
						}),
						rut, drut, res, dvi;

					if (v.length === 0 || v.length < 2 || u.length === 0) {
						return false;
					}

					rut = v.substring(0, v.length - 1);
					drut = v.substring(v.length - 1);

					for (i = rut.length - 1; i >= 0; i -= 1) {
						sum = sum + rut.charAt(i) * mul;
						if (mul === 7) {
							mul = 2;
						} else {
							mul += 1;
						}
					}

					res = sum % 11;
					dvi = 11 - res;
					dvr = res === 1 ? "k" : (res === 0 ? "0" : dvi.toString());

					if (dvr !== drut.toLowerCase()) {
						return false;
					}
				
					if (
						v.length < 8 ||
						v === "111111111" ||
						v === "222222222" ||
						v === "333333333" ||
						v === "444444444" ||
						v === "555555555" ||
						v === "666666666" ||
						v === "777777777" ||
						v === "888888888" ||
						v === "999999999"
					) {
						return false;
					}
				
					return true;
				},
				email = function (valor) {
					let re = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					if (valor.length < 6 || !re.test(valor)) {
						return false;
					}
					return true;
				},
				nombre = function (valor) {
					let splitted = valor.toLowerCase().split(""),
						validos = "abcdefghijklmnñopqrstuvwxyzàáäéèëìíïòòöùúü' -",
						checkCaracteres = function () {
							splitted.forEach(function (value, index) {
								if (validos.indexOf(value) === -1) {
									return false;
								}
							});
							return true;
						};
					if (!checkCaracteres() || valor.length < 3) {
						return false;
					}
					return true;
				},
				apellidos = function (valor) {
					let splitted = valor.toLowerCase().split(""),
						validos = "abcdefghijklmnñopqrstuvwxyzàáäéèëìíïòòöùúü' -",
						checkCaracteres = function () {
							splitted.forEach(function (value, index) {
								if (validos.indexOf(value) === -1) {
									return false;
								}
							});
							return true;
						};
					if (valor.indexOf(" ") === -1 || !checkCaracteres() || valor.length < 2) {
						return false;
					}
					return true;
				},
				telefono = function (valor) {
					let re = /^[0-9]+$/;
					if (valor.length < 8 || !re.test(valor)) {
						return false;
					}
					return true;
				},
				patente = function (valor) {
					let consonantes = "bcdfghjklmnpqrstvwxyz",
						vocales = "aeiou",
						numeros = "0123456789",
						letras = consonantes + vocales,
						patente = valor.toLowerCase().split(""),
						resolverAntigua = function (aP) {
							if (letras.indexOf(aP[0]) === -1 || letras.indexOf(aP[1]) === -1) {
								return false;
							} else if (numeros.indexOf(aP[2]) === -1 || numeros.indexOf(aP[3]) === -1 || numeros.indexOf(aP[4]) === -1 || numeros.indexOf(aP[5]) === -1) {
								return false;
							} else {
								return true;
							}
						},
						resolverNueva = function (aP) {
							if (consonantes.indexOf(aP[0]) === -1 || consonantes.indexOf(aP[1]) === -1 || consonantes.indexOf(aP[2]) === -1 || consonantes.indexOf(aP[3]) === -1) {
								return false;
							} else if (numeros.indexOf(aP[4]) === -1 || numeros.indexOf(aP[5]) === -1) {
								return false;
							} else {
								return true;
							}
						};
					if ((resolverAntigua(patente) === true || resolverNueva(patente) === true) && patente.length === 6) {
						return true;
					}
					return false;
				},
				numero = function (valor) {
					return !isNaN(parseInt(valor, 10));
				},
				generico = function (valor) {
					return valor.length >= 1;
				},
				anno = function (valor) {
					let v = typeof valor === "string" ? parseInt(valor, 10) : valor;
					return v > 1900 && v < 2020;
				},
				v = typeof valor === "number" ? valor.toString() : valor,
				a;

			if (valor === undefined) {
				return false;	
			}
			
			switch (tipo) {
				case "rut":
					a = rut(v);
					break;
				case "email":
					a = email(v);
					break;
				case "nombre":
					a = nombre(v);
					break;
				case "apellidos":
					a = apellidos(v);
					break;
				case "phone":
				case "celular":
				case "tel":
				case "telefono":
					a = telefono(v);
					break;
				case "patente":
					a = patente(v);
					break;
				case "number":
				case "numero":
					a = nombre(v);
					break;
				case "anno":
				case "año":
					a = anno(v);
					break;
				default:
					a = generico(v);
					break;
				}
			return a;
		},
		validacionExpress: function (clave, tipo, valor) {
			clave.status = valor !== undefined ? this.validar(tipo, valor) : false;
			
			if (clave.status) {
				if (tipo === "rut") {
					clave.value = clave.value.replace(/\./g, "").replace(/\ /g, "").replace(/\-/g, "");
					clave.value = clave.value.substr(0, clave.value.length - 1) + "-" + clave.value.substr(clave.value.length - 1).trim();
				}
				else if (tipo === "patente") {
					clave.value = clave.value.toUpperCase().trim();
				}
			}
		},
		getMensajeError: function (key) {
			var k = key.toLowerCase().replace(/\-/g, "_").replace(/\ /g, "_").replace(/\./g, "_");
			
			switch (k) {
				case "nombre":
					return "Ingrese su nombre y al menos un apellido";
				case "rut":
					return "Ingrese un RUT válido";
				case "email":
					return "Ingrese un email válido";
				case "tel":
				case "telefono":
					return "Ingrese un número de teléfono válido";
				case "sexo":
					return "Seleccione su sexo";
				case "nacionalidad":
					return "Ingrese su nacionalidad";
				case "fecha_nacimiento":
					return "Ingrese su fecha de nacimiento";
				case "renta":
					return "Seleccione un rango de renta";
				case "valor_vivienda":
					return "Seleccione un rango del valor de la vivienda";
			}
		},
		validarYEnviar: function (form_id, form, solo_retornar) {
			let estado = true,
				t = this,
				errores = [],
				formulario,
				key;
			
			if (form.indexOf(".") >= 0) {
				let f = form.split(".");
				formulario = t;
				f.forEach(function (v) {
					formulario = formulario[v];
				});
			} else {
				formulario = this[form];
			}

			for (key in formulario) {
				if (formulario.hasOwnProperty(key)) {
					let status = formulario[key].hasOwnProperty("validacion") ? this.validar(formulario[key].validacion, formulario[key].value) : true;
					formulario[key].status = status;
					
					if (!formulario[key].status) {
						estado = false;
					}
					
					if (!status) {
						errores.push(this.getMensajeError(key));
					}
				}
			}
			if (!estado) {
				errores = errores.filter(function (v) {
					return v !== undefined && v !== "";
				});
				
				if (errores.length > 0) {
					alert("Su formulario contiene los siguientes errores:\n -" + errores.join("\n- "));
				} else {
					alert("Su formulario contiene errores. Por favor, corríjalos e intente nuevamente.");
				}
			} else {
				if (typeof solo_retornar !== "undefined") {
					if (solo_retornar) {
						return true;
					} else {
						document.getElementById(form_id).submit();
					}
				} else {
					document.getElementById(form_id).submit();
				}
			}
		},
		rescatarDatosFormulario: function (objeto_formulario, append) {
			var salida = {};
			for (let key in objeto_formulario) {
				if (objeto_formulario.hasOwnProperty(key)) {
					salida[key] = objeto_formulario[key].value;
				}
			}
			
			if (typeof append === "object") {
				for (let key in append) {
					if (append.hasOwnProperty(key)) {
						salida[key] = append[key];
					}
				}
			}
			
			return salida;
		},
		//forms endregion
		
		//generales region
		getSrcset: gen_methods.getSrcset,
		getSrc: gen_methods.getSrc,
		placeholder: gen_methods.placeholder,
		toggleModalOpened: gen_methods.toggleModalOpened
		//generales endregion
	},
	filters: {
		capitalize: filtros.capitalize,
		noSpaces: filtros.noSpaces,
		numero: filtros.numero,
		onlyNumbers: filtros.onlyNumbers
	},
	mounted: function () {}
});












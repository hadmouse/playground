<?php
//Template name: Feedback Pago
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors', 1);
require_once(get_template_directory() . "/webpay/api/webpay.php");


get_header(); ?>

<main class="main-main main-main--feedback">
	
<?php 
	
		$token = (isset($_REQUEST["token_ws"]) ? $_REQUEST["token_ws"] : NULL);
		if (isset($_REQUEST["TBK_TOKEN"])) {
			$token = $_REQUEST["TBK_TOKEN"];
		}
		$webpay = new WebPay();
		$webpay->loadToken($token);
		$console = json_encode($webpay);
		
		if ($webpay->TBK_RESPUESTA != 0) {
			$webpay->notificaError();
	?>
		<article class="feedback feedback--erroneo text-center">
			<div class="feedback_inner">
				<h3>Transacción Rechazada</h3>
				<?php  if (strlen($webpay->OC) > 0): ?>
				<p>
					Orden de Compra <span id="orden-de-compra"><?php echo $webpay->OC; ?></span>
				</p>
				<?php endif; ?>
				<p>
					Las posibles causas de este rechazo son:
				</p>
				<ul class="clean-list">
					<li>Error en el ingreso de los datos de su tarjeta de crédito o débito (fecha y/o código de seguridad).</li>
					<li>Su tarjeta de crédito o débito no cuenta con el cupo necesario para cancelar la compra.</li>
					<li>Tarjeta aún no habilitada en el sistema financiero.</li>
				</ul>
				<div class="text-center feedback_botones">
					<a href="<?php echo get_home_url(); ?>" class="btn btn-color-1">Volver a intentarlo <span class="fa fa-home"></span></a>
				</div>

			</div>
		</article>
	<?php
		} else {
			$webpay->notificaCierre();
			$dias = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
			$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
			$reserva = getReserva($_SESSION["last_id"]);
			
			$fecha_dia_semana = $dias[ intval( date("N", strtotime($reserva["fecha"])) ) - 1 ];
			$fecha_dia = date("j", strtotime( $reserva["fecha"] ));
			$fecha_mes = $meses[ intval(date("n", strtotime($reserva["fecha"]))) - 1 ];
			
			$hora = explode(":", $reserva["hora"]);
			unset($hora[2]);
			$hora = implode(":", $hora);
								
	?>
		<article class="feedback feedback--exitoso text-center background-color-2">
			<div class="feedback_inner">
				<h3 class="color-4 text-left margin-bottom-40 font-22" style="font-weight: 500;">Tu reserva se ha procesado correctamente</h3>
				<p class="luft-check font-10-em text-center color-1 margin-top-20 margin-bottom-10 line-height-0"></p>
				<p class="text-center color-1 font-19 margin-top-10 margin-bottom-60">
					<?php echo $fecha_dia_semana . " " . $fecha_dia . " de " . $fecha_mes; ?>, <?php echo $hora; ?> hrs
					<br>
					<?php echo $reserva["servicio"]; ?>
				</p>
			
				<table class="table">
					<tbody>
						<tr>
							<td>Número de Orden de Compra</td>
							<td id="numero-orden-compra"><?php echo $webpay->OC; ?></td>
						</tr>
						<tr>
							<td>Nombre del comercio</td>
							<td id="nombre-comercio">Luft Car Wash</td>
						</tr>
						<tr>
							<td>Monto y moneda de la transacción</td>
							<td id="monto-moneda">$ <?php
							echo number_format($webpay->monto,0,".",".");
							?> pesos chilenos</td>
						</tr>
						<tr>
							<td>Código de autorización de la transacción</td>
							<td id="codigo-autorizacion"><?php echo $webpay->authCode; ?></td>
						</tr>
						<tr>
							<td>Fecha de la transacción</td>
							<td id="fecha-transaccion"><?php echo $webpay->fecha; ?></td>
						</tr>
						<tr>
							<td>Tipo de Transacci&oacute;n</td>
							<td>Compra</td>
						</tr>
						<tr>
							<td>Tipo de pago realizado (Débito o Crédito)</td>
							<td id="tipo-pago"><?php echo $webpay->getTBK_TIPO_PAGO(); ?></td>
						</tr>
						<tr>
							<td>Tipo de cuota</td>
							<td id="tipo-cuota"><?php echo $webpay->getTipoCuota(); ?></td>
						</tr>
						<tr>
							<td>Cantidad de cuotas</td>
							<td id="cantidad-cuotas"><?php echo $webpay->cuotas; ?></td>
						</tr>
						<tr>
							<td>4 últimos dígitos de la tarjeta bancaria</td>
							<td id="ultimos-digitos"><?php echo $webpay->finalTarjeta; ?></td>
						</tr>
						<tr>
							<td>Descripción de los bienes y/o servicios</td>
							<td id="descripcion-bienes">Pago de cuota crédito</td>
						</tr>
					</tbody>
				</table>
				<div class="text-center feedback_botones">
					<a href="<?php echo get_home_url(); ?>" class="btn btn-color-1">Volver al inicio <span class="fa fa-home"></span></a>
				</div>
			</div>
		</article>
		
		<section class="home-section">
			<a class="btn btn-color-5 btn-lg btn-block no-margin-bottom clear-after" href="https://wa.me/<?php echo str_replace("+", "", get_option("custom_option_whatsapp")); ?>">
				<div class="float-3">
					<span class="font-3-5-em line-height-0 luft-whatsapp"></span>
				</div>
				<div class="float-9">
					<p class="font-weight-500 margin-bottom-0" style="font-size: 20px;">¿Tienes dudas?</p>
					<p class="margin-top-0" style="font-size: 20px;">Contáctanos en WhatsApp</p>
				</div>
			</a>
		</section>
		
	<?php
		};
	?>
</main>

<?php
get_footer();
